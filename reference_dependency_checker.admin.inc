<?php 

/**
 * @file
 * Administration functions for the reference dependency checker.
 */

/**
 * Implements hook_form().
 */
function reference_dependency_checker_settings_form($node, &$form_state) {
  $form = array();
  
  $form['reference_dependency_checker_live_revision'] = array(
    '#type' => 'checkbox',
  	'#title' => t('Only check the live revision for a reference?'),
    '#default_value' => variable_get('reference_dependency_checker_live_revision', 1),
  );
  
  $form['reference_dependency_checker_published'] = array(
    '#type' => 'checkbox',
  	'#title' => t('Only check published nodes for a reference?'),
    '#default_value' => variable_get('reference_dependency_checker_published', 1),
  );
  
  $form['reference_dependency_checker_force_edit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force the editor to edit the linked nodes before deleting the node?'),
    '#default_value' => variable_get('reference_dependency_checker_force_edit', 1),
  );
  
  return system_settings_form($form);
}
